# Env builder

## Inputs

This can be used to determine if a env map should be created if not null will be returned.

```terraform
variable "create" {
  description = "A boolean to instruct the module whether or not to create the output"
  type        = bool
  default     = true
}
```

The environment to select sub map type objects.
```terraform
variable "env" {
  description = "The environment the variables are to be used in"
  type        = string
  default     = "dev"
}
```

```terraform
variable "env_vars" {
  description = "A map of environment variables"
  type        = any
  default     = {}
}
```


## Outputs

```terraform
output "env" {
  description = "A flat map of environment variables"
  value       = local.env_map
}
```

# Environment Mapping

The module uses the env variable to map the correct env var to the correct input.
See the example below.

# Usage

```terraform
module "api_env" {
  source   = "git::https://code.vt.edu/nis-terraform/modules/env-builder.git"
  env      = var.env
  env_vars = {
    NODE_ENV = var.env
    DB_CONNECTION_STRING = {
      dev     = "db-dev.cloud.es.vt.edu"
      preprod = "db-ppd.cloud.es.vt.edu"
      prod    = "db-prod.cloud.es.vt.edu"
    }
  }
}
```

The module can then be referenced in terraform like so

```terraform
  module.api_env.env
```

The resulting map from the example above would be

```
{
  NODE_ENV             = "preprod"
  DB_CONNECTION_STRING = "db-ppd.cloud.es.vt.edu"
}
```

Then using the templatefile function your could iterate over your map and create an .env
file like so:

outputs.tf

```terraform
output "helm_release" {
  description = "A template file to build a helm release"
  value = templatefile("${path.module}/templates/helm-release.tpl", {
    k8_namespace          = var.k8_namespace
    chart_version         = local.chart_version
    release_name          = var.release_name
    service_account_role  = aws_iam_role.k8_service_account_role
    env                   = module.api_env.env
  })
}
```

templates/helm-release.tpl

```go
...
apiVersion: helm.toolkit.fluxcd.io/v2beta1
kind: HelmRelease
metadata:
  name: "${release_name}"
spec:
  targetNamespace: "${k8_namespace}"
  serviceAccountName: flux
  interval: 5m
  chart:
    spec:
      chart: nis-customer-portal
      version: "${chart_version}"
      sourceRef:
        kind: HelmRepository
        name: nis-customer-portal
        namespace: "${k8_namespace}"
      interval: 1m
  values:
    serviceAccountArn: "${service_account}"
    serviceAccountRole: "${service_account_role}"
    appName: "${app_name}"
    env:
%{ for key, value in env ~}
      ${key}: "${value}"
%{ endfor }
```

When the deploy step of the pipeline runs you can then cat out your helm release manifest like so

```
  gitlab-terraform output -raw helm_release > /tmp/repo-files/${CI_COMMIT_REF_SLUG}.yml
```