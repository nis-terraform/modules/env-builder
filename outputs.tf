output "env" {
  description = "A flat map of environment variables"
  value       = var.create ? local.env_map : null
}