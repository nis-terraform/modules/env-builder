variable "create" {
  description = "A boolean to instruct the module whether or not to create the output"
  type        = bool
  default     = true
}

variable "env" {
  description = "The environment the variables are to be used in"
  type        = string
  default     = "dev"
}
variable "env_vars" {
  description = "A map of environment variables"
  type        = any
  default     = {}
}

# This for loop iterates over an object and picks the dev or prod key and value and creates a flattened object of env varables for that env.
locals {
  env_map = var.create ? { for key, value in var.env_vars : key => try(tostring(value), value[var.env]) } : null
}
